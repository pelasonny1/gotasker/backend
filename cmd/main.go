package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"GOTASKER/internal/db"
	"GOTASKER/internal/task"

	"github.com/go-kit/kit/log"
)

func main() {
	var logger log.Logger
	logger = log.NewLogfmtLogger(os.Stderr)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = log.With(logger, "caller", log.DefaultCaller)

	database, err := db.Connect()
	if err != nil {
		logger.Log("connect_error", err)
		os.Exit(1)
	}
	defer database.Close()

	repo := task.NewRepository(database)

	svc := task.NewService(repo)
	svc = task.LoggingMiddleware(logger)(svc)

	endpoints := task.MakeEndpoints(svc)

	httpHandler := task.NewHTTPHandler(endpoints, logger)

	httpAddr := ":8080"
	httpListener, err := net.Listen("tcp", httpAddr)
	if err != nil {
		logger.Log("listen_error", err)
		os.Exit(1)
	}

	errc := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errc <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		logger.Log("transport", "HTTP", "address", httpAddr, "msg", "listening")
		errc <- http.Serve(httpListener, httpHandler)
	}()

	logger.Log("exit", <-errc)
}

func decodeGetTasksRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return task.GetTasksRequest{}, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}
