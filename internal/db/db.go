package db

import (
	"database/sql"

	_ "github.com/lib/pq"
)

func Connect() (*sql.DB, error) {
	connStr := "postgres://usuario:asd@localhost/gotaskerdb?sslmode=disable"
	return sql.Open("postgres", connStr)
}
