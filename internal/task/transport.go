package task

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-kit/kit/log"
	kitHttp "github.com/go-kit/kit/transport/http"
)

func NewHTTPHandler(endpoints Endpoints, logger log.Logger) http.Handler {
	m := http.NewServeMux()
	m.Handle("/tasks", kitHttp.NewServer(endpoints.GetTasksEndpoint, decodeGetTasksRequest, encodeResponse))
	m.Handle("/tasks/create", kitHttp.NewServer(endpoints.CreateTaskEndpoint, decodeCreateTaskRequest, encodeResponse))
	return m
}

func decodeGetTasksRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return GetTasksRequest{}, nil
}

func decodeCreateTaskRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req CreateTaskRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
