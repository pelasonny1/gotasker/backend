package task

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

// Endpoints struct agrega todos los endpoints disponibles para el servicio de tareas.
// Agruparlos de esta manera facilita la gestión y configuración de los mismos.
type Endpoints struct {
	GetTasksEndpoint   endpoint.Endpoint
	CreateTaskEndpoint endpoint.Endpoint
	UpdateTaskEndpoint endpoint.Endpoint
	DeleteTaskEndpoint endpoint.Endpoint
}

// MakeEndpoints inicializa y retorna una estructura de Endpoints.
// Esta función centraliza la creación de todos los endpoints del servicio,
// lo que permite una fácil modificación y mantenimiento.
func MakeEndpoints(s Service) Endpoints {
	return Endpoints{
		CreateTaskEndpoint: MakeCreateTaskEndpoint(s),
		GetTasksEndpoint:   MakeGetTasksEndpoint(s),
		UpdateTaskEndpoint: MakeUpdateTaskEndpoint(s),
		DeleteTaskEndpoint: MakeDeleteTaskEndpoint(s),
	}
}

// MakeGetTasksEndpoint configura el endpoint para obtener una lista de tareas.
// Este endpoint maneja la solicitud de lista de tareas, realizando una llamada
// al servicio y devolviendo las tareas en forma de respuesta estructurada.
func MakeGetTasksEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		tasks, err := svc.GetTasks(ctx)
		if err != nil {
			// Proporcionar más contexto cuando no se pueden obtener las tareas
			return nil, err
		}
		return GetTasksResponse{Tasks: tasks}, nil
	}
}

// MakeCreateTaskEndpoint define cómo se crea una nueva tarea.
// Acepta una descripción a través de la solicitud, y devuelve una nueva tarea
// con un identificador único.
func MakeCreateTaskEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateTaskRequest)
		id, err := svc.CreateTask(ctx, req.Description)
		if err != nil {
			// Proporcionar más contexto si falla la creación de la tarea
			return nil, err
		}
		return CreateTaskResponse{ID: id}, nil
	}
}

// MakeUpdateTaskEndpoint configura el endpoint para actualizar detalles específicos de una tarea.
// Requiere que la ID de la tarea, la descripción y el estado de completado sean especificados.
func MakeUpdateTaskEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(UpdateTaskRequest)
		err := svc.UpdateTask(ctx, req.ID, req.Description, req.Completed)
		if err != nil {
			// Manejar errores específicos de actualización
			return nil, err
		}
		return UpdateTaskResponse{Success: true}, nil
	}
}

// MakeDeleteTaskEndpoint maneja la eliminación de una tarea dado un ID específico.
// Confirma la acción mediante una respuesta de éxito.
func MakeDeleteTaskEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteTaskRequest)
		err := svc.DeleteTask(ctx, req.ID)
		if err != nil {
			// Explicar qué errores podrían ocurrir durante la eliminación
			return nil, err
		}
		return DeleteTaskResponse{Success: true}, nil
	}
}

// Definiciones de tipos para solicitudes y respuestas para facilitar la manipulación y entender el flujo de datos.
type GetTasksRequest struct{}
type GetTasksResponse struct {
	Tasks []Task `json:"tasks"`
}
type CreateTaskRequest struct {
	Description string `json:"description"`
}
type CreateTaskResponse struct {
	ID string `json:"id"`
}
type UpdateTaskRequest struct {
	ID          string `json:"id"`
	Description string `json:"description"`
	Completed   bool   `json:"completed"`
}
type UpdateTaskResponse struct {
	Success bool `json:"success"`
}
type DeleteTaskRequest struct {
	ID string `json:"id"`
}
type DeleteTaskResponse struct {
	Success bool `json:"success"`
}
