package task

import (
	"context"
)

type Task struct {
	ID          string `json:"id"`
	Description string `json:"description"`
	Completed   bool   `json:"completed"`
}

type Repository interface {
	CreateTask(ctx context.Context, task Task) error
	GetTasks(ctx context.Context) ([]Task, error)
	UpdateTask(ctx context.Context, task Task) error
	DeleteTask(ctx context.Context, id string) error
}

type Service interface {
	CreateTask(ctx context.Context, description string) (string, error)
	GetTasks(ctx context.Context) ([]Task, error)
	UpdateTask(ctx context.Context, id string, description string, completed bool) error
	DeleteTask(ctx context.Context, id string) error
}

type service struct {
	repo Repository
}

func NewService(repo Repository) Service {
	return &service{repo}
}

func (s *service) CreateTask(ctx context.Context, description string) (string, error) {
	task := Task{
		Description: description,
		Completed:   false,
	}
	err := s.repo.CreateTask(ctx, task)
	if err != nil {
		return "", err
	}
	return task.ID, nil
}

func (s *service) GetTasks(ctx context.Context) ([]Task, error) {
	return s.repo.GetTasks(ctx)
}

func (s *service) UpdateTask(ctx context.Context, id string, description string, completed bool) error {
	task := Task{
		ID:          id,
		Description: description,
		Completed:   completed,
	}
	return s.repo.UpdateTask(ctx, task)
}

func (s *service) DeleteTask(ctx context.Context, id string) error {
	return s.repo.DeleteTask(ctx, id)
}
