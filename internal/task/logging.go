package task

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
)

type Middleware func(Service) Service

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next Service) Service {
		return &loggingMiddleware{next, logger}
	}
}

type loggingMiddleware struct {
	next   Service
	logger log.Logger
}

func (mw loggingMiddleware) CreateTask(ctx context.Context, description string) (string, error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "CreateTask", "description", description, "took", time.Since(begin))
	}(time.Now())
	return mw.next.CreateTask(ctx, description)
}

func (mw loggingMiddleware) GetTasks(ctx context.Context) ([]Task, error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetTasks", "took", time.Since(begin))
	}(time.Now())
	return mw.next.GetTasks(ctx)
}

func (mw loggingMiddleware) UpdateTask(ctx context.Context, id string, description string, completed bool) error {
	defer func(begin time.Time) {
		mw.logger.Log("method", "UpdateTask", "id", id, "description", description, "completed", completed, "took", time.Since(begin))
	}(time.Now())
	return mw.next.UpdateTask(ctx, id, description, completed)
}

func (mw loggingMiddleware) DeleteTask(ctx context.Context, id string) error {
	defer func(begin time.Time) {
		mw.logger.Log("method", "DeleteTask", "id", id, "took", time.Since(begin))
	}(time.Now())
	return mw.next.DeleteTask(ctx, id)
}
