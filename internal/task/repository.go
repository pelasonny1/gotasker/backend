package task

import (
	"context"
	"database/sql"
)

type repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return &repository{db}
}

func (r *repository) CreateTask(ctx context.Context, task Task) error {
	query := `INSERT INTO tasks (description, completed) VALUES ($1, $2) RETURNING id`
	return r.db.QueryRowContext(ctx, query, task.Description, task.Completed).Scan(&task.ID)
}

func (r *repository) GetTasks(ctx context.Context) ([]Task, error) {
	tasks := []Task{}
	query := `SELECT id, description, completed FROM tasks`
	rows, err := r.db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var task Task
		if err := rows.Scan(&task.ID, &task.Description, &task.Completed); err != nil {
			return nil, err
		}
		tasks = append(tasks, task)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return tasks, nil
}

func (r *repository) UpdateTask(ctx context.Context, task Task) error {
	query := `UPDATE tasks SET description = $1, completed = $2 WHERE id = $3`
	_, err := r.db.ExecContext(ctx, query, task.Description, task.Completed, task.ID)
	return err
}

func (r *repository) DeleteTask(ctx context.Context, id string) error {
	query := `DELETE FROM tasks WHERE id = $1`
	_, err := r.db.ExecContext(ctx, query, id)
	return err
}
